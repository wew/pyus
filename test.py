import pyus.data.io as dio
import numpy as np

filename = "20220930_123136785"
filedir = "/datacommons/ultrasound/wew12/Data/Muscle/20220930_planewaves_L7-4/acqhold_74_4MHz"
params = dio.loadmat(filename=filename, filedir=filedir)

shape = np.array([
    params['PData']['Size'][0], 
    params['PData']['Size'][1], 
    params['params']['multi']['na'], 
    params['params']['multi']['nreps']], 
    dtype=int)

I, Q = dio.loadSubsetIQ(filename=filename, dims=shape, shape=shape, axis = 2, frames=[0, (params['params']['multi']['na']-1)/2, params['params']['multi']['na']-1], filedir=filedir)

import matplotlib.pyplot as plt

env=np.sqrt(np.sum(I[:,:,:,0], axis=2)**2 + np.sum(Q[:,:,:,0], axis=2)**2)

plt.figure()
plt.imshow(20*np.log10(env))
plt.show()