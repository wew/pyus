# pythontools
Commonly used tools for image processing and general ultrasound in python. 

# Table of contents
 - [How to install](#install-the-package)
 - [Project Organization](#project-organization)
 - [Version History](#version-history)

# Install the package

First, clone the package...
```
git clone https://gitlab.oit.duke.edu/wew/pythontools.git
```

## Install in development mode
```
cd pythontools
python -m pip install -e .
```

## Install to use
```
cd pythontools
// Activate your chosen venv
python -m pip install -e .
```

# Project Organization
This package is written to handle general computational tasks used in ultrasound imaging. These methods are organized into three main submodules: `improc`, `dataio`, `ustools`

### Project Structure

```
| pythontools
  - | improc
      - | sampling
  - | dataio
  - | ustools
      - | bmfrm
      - | dispest
```

## improc
This collection of modules is used for general image processing tasks. Current functionality includes upsampling and filtering.

## dataio
This collection of modules reads and formats datafiles based on standard binary files and HDF5 parameter files

## ustools
This collection of modules contains tools to complete standard tasks in ultrasound

# Version History
## v0.0.2
First version with beamforming and demodulation (located inside of ustools)

## v0.0.1
Initial version, solely implemented dataio and general image processing

# Contributors
 - Wren Wightman, @wew, wren.wightman@duke.edu
