import numpy as np
import scipy.signal as sig

def upsample(data, usfs, window='tukey', mode:str='numpy'):
    """Upsample a tensor of data by the given upsample factors
    
    Parameters:
        data: a tensor of data to be upsampled
        usfs: up-sample factors for each dimension
        mode: treats data like 'numpy' array or an 'xarray'
    Returns:
        usdata: the upsampled data, of size data.dims * usfs
    """
    usfs = np.array(usfs)
    if mode == 'numpy':
        return _upsample_numpy(data=data, usfs=usfs, window=window)
    elif mode == 'xarray':
        raise Exception("Upsampling wrapper has not been implemented for xarrays")

def _upsample_numpy(data, usfs, window):
    """Upsample a tensor of data by the given upsample factors
    
    Parameters:
        data: a tensor of data to be upsampled
        usfs: up-sample factors for each dimension
        window: window used to reduce ringing
    Returns:
        usdata: the upsampled data, of size data.dims * usfs
    """
    # check that the dimensions are valid
    if not len(data.shape) == len(usfs):
        raise Exception(f"Number of dims in data (n={len(data.shape)}) must match num dims specified in usfs (n={len(usfs)}).")
    N = len(usfs)

    # verify that upsample factors are all integers
    for usf in usfs:
        if usf < 1:
            raise Exception(f"Upsample factors must be >= 1. {usf} is not valid.")

    # calculate the dimensions of 
    origdims = data.shape
    newdims = np.array([int(val) for val in origdims * np.array(usfs)])

    #resample along each sample length that changes
    usdata = data
    for ind in range(N):
        if origdims[ind] != newdims[ind]:
            usdata =  sig.resample(x=usdata, num=newdims[ind], axis=ind, window=window)
    
    return usdata

def imwarp(im, T, input=None, output=None, k=3, fill=None):
    """Warp image via an affine transformation

    Paramaters:
    ----
        im: input image, 2D, existing in the space defined by 
        T: 3 by 3 transformation matrix from input space to output space
        input: None or description of input image as a dict('px':(N, M), 'extent':(minx, maxx, miny, maxy))
        output: None or description of output image as a dict('px':(N, M), 'extent':(minx, maxx, miny, maxy))
        k: order of polynomial used in approximation
    
    Returns:
    ----
        image on the domain defined by output

    Raises:
    ----
        ValueError: if ndim of im > 2
        ValueError: if dimensions defined by input do not match im

    Notes:
    ----
        if 'input' is None, assumes upper left corner is (0,0) and 'extent':(0,M,0,N)
        if 'output' is None, copies the domain defined by input
    """
    from scipy.interpolate import RectBivariateSpline as RBS

    # Verify image is valid
    im  = im.squeeze()
    if not np.ndim(im) == 2:
        raise ValueError(f"im must have 2 dimensions, not {np.ndim(im)}")
    (N, M) = im.shape

    T = T.squeeze()
    if (not np.ndim(T) == 2) or (not T.shape[0] == 3) or (not T.shape[1] == 3):
        raise ValueError(f"T must be a 3 by 3 matrix")

    # Autofill input or verify it if given
    if input is None:
        input = {}
        input['extent'] = (0, M, 0, N)
    input['px'] = (N, M)

    # copy input domain if output is not given
    if output is None:
        output = dict(input)

    # generate coordinates of input image pixels
    in_y = np.linspace(input['extent'][2], input['extent'][3], input['px'][0])
    in_x = np.linspace(input['extent'][0], input['extent'][1], input['px'][1])

    # generate coordinates of output image pixels
    out_y = np.linspace(output['extent'][2], output['extent'][3], output['px'][0])
    out_x = np.linspace(output['extent'][0], output['extent'][1], output['px'][1])

    # generate interpolator of grid data
    f = RBS(in_y, in_x, im, kx=k, ky=k)

    # generate coords to interpolate (Fortran order)
    y = np.tile(out_y, output['px'][1])
    x = np.repeat(out_x, output['px'][0])
    point_vec = np.array([x, y, np.ones_like(x)])

    # convert coords from output domain into input domain
    out2in = np.linalg.inv(T) @ point_vec

    # get values out output domain
    z = f(out2in[1,:], out2in[0,:], grid=False)
    
    if fill is not None:
        invalid_x = (out2in[0,:] < np.min(input['extent'][:2])) | (out2in[0,:] > np.max(input['extent'][:2]))
        invalid_y = (out2in[1,:] < np.min(input['extent'][2:])) | (out2in[1,:] > np.max(input['extent'][2:]))
        invalid = invalid_x | invalid_y
        
        # set those values to fill
        z[invalid] = fill

    im_out = z.reshape(output['px'], order='F')

    return im_out