"""
name: filetrees
this package is used to automatically traverse file trees
"""
import os

class FileTreeMeta():
    """Builds a file tree and extracts metadata from file structure name

    Params:
    ----
        root: root directory of this branch of the tree
        metasep: the seperator between the metadata and directory name
        level: tree level. Do not need to alter
    """
    def __init__(self, root, metasep='_', level=0, metatree={}):
        self.root = root
        self.metasep = metasep
        self.metatree = dict(metatree)
        self.level = level
        self.children = []
        self.files = []
        self.loadTree()

    def loadTree(self):
        self.extractLocalMeta()
        for item in os.listdir(self.root):
            itempath = os.path.join(self.root, item)
            if os.path.isdir(itempath):
                child = FileTreeMeta(itempath, metasep=self.metasep, level=self.level+1, metatree=self.metatree)
                self.children.append(child)
            else:
                self.files.append(itempath)

    def extractLocalMeta(self):
        # Extract the appropriate metadata depending on if the root is terminated with the path seperator
        if self.root[-1] == os.sep:
            meta_tag = self.root.split(os.sep)[-2]
        else:
            meta_tag = self.root.split(os.sep)[-1]

        # Save the metaata
        if self.level == 0:
            self.meta = meta_tag
            self.metatree['basemeta'] = meta_tag
        else:
            chunks = meta_tag.split(self.metasep)
            if len(chunks) == 1:
                self.meta = chunks[0]
                self.metatree[chunks[0]] = chunks[0]
            elif len(chunks) == 2:
                self.meta = chunks[1]
                self.metatree[chunks[0]] = chunks[1]
            else:
                raise Exception('Substructure of filetree has more than two meta-data blocks')

    def getroot(self):
        return self.root
    
    def getfilenames(self, suffix = '.', children = False):
        if children:
            names=[]
            roots=[]
            for file in self.files:
                file_w_ex = file.rsplit(os.sep, 1)[-1]
                name = file_w_ex.rsplit(suffix, 1)[0]
                if not name == file_w_ex:
                    names.append(name)
                    roots.append(self.root)
            for child in self.children:
                data = child.getfilenames(suffix=suffix, children=children)
                if (data is not None) and (len(data) == 2):
                    roots.extend(data[0])
                    names.extend(data[1])

            return roots, names
        else:
            names=[]
            for file in self.files:
                file_w_ex = file.rsplit(os.sep, 1)[-1]
                name = file_w_ex.rsplit(suffix, 1)[0]
                if not name == file_w_ex:
                    names.append(name)
            return self.root, names
    
    def __str__(self) -> str:
        tostr = ""
        indent = "\t" * self.level
        tostr += f"{indent}Root: {self.root}\n"
        tostr += f"{indent}- Metadata Tag: {self.meta}\n"
        tostr += f"{indent}- Files ({len(self.files)}):\n"
        for file in self.files:
            tostr += f"{indent}    {file}\n"
        tostr += f"{indent}- Children ({len(self.children)}):\n"
        for child in self.children:
            tostr += f"{str(child)}\n"
        return tostr