"""
This module contains wrappers for various beamforming functions

Author: Wren Wightman (wren.wightman@duke.edu)
"""
import logging
import numpy as np
logger = logging.getLogger(__name__)

def demodulate(data, axis, fd, fc, fs, usf=int(1), dsf=int(1), tstart=0, mode='numpy', filter='tukey'):
    """Demodulate an rf signal
    
    Parameters:
        data: tensor of RF data (xarray or numpy)
        axis: the axis over which to demodulate
        fd: the demodulation frequency [Hz] (float or array: len(array) = data.shape[axis])
        fc: cuttoff frequency of filter [Hz]
        fs: sampling frequency along 'axis'
        usf: relative to input
        dsf: relative to upsampled data
        mode: 'numpy' or 'xarray'
        filter: the type of low pass filter used
    Outputs:
        I: the in-phase component of signal
        Q: the quadrature component of signal
    """

    if mode == 'numpy':
        try:
            from pyus.ustools.bmfrm._bmfrm_helpers import _demodulate_numpy
            from scipy.signal import resample
            if (dsf == 1) and (usf ==1):
                return _demodulate_numpy(data, fd, fs, fc, tstart, axis, filter=filter)
            else:
                # upsample data 
                t_norm = tstart + np.arange(data.shape[axis])/fs
                dataprime, tprime = resample(data, int(usf*data.shape[axis]), t_norm, window=filter)
                fsprime = 1/np.mean(np.diff(tprime))

                # demodulate
                I, Q = _demodulate_numpy(dataprime, fd, fsprime, fc, tstart, axis, filter=filter)

                slicer = [slice(data.shape[axind]) if not (axind == axis) else slice(0, dataprime.shape[axind], dsf) for axind in range(len(data.shape))]
                # downsample
                return I[tuple(slicer)], Q[tuple(slicer)]
        except Exception as e:
            logger.warn("Error occured when demodulating in numpy mode")
            print(e)
            raise e
    elif mode == 'xarray':
        try:
            from pyus.ustools.bmfrm._bmfrm_helpers import _demodulate_xarray
            return _demodulate_xarray(data, fd, fs, tstart, axis, filter=filter)
        except Exception as e:
            logger.warn("Error occured when demodulating in xarray mode")
            raise e
    else:
        raise Exception(f"{mode} is not a valid demodulation call. Must be 'numpy' or 'xarray'")

def remodulate(I, Q, axis, fd, fc, fs, usf = 1, tstart = 0, mode='numpy', filter='tukey', window='tukey'):
    """Demodulate an rf signal
    
    Parameters:
        I: tensor of in-phase data (xarray or numpy)
        Q: tensor of quadrature data (xarray or numpy)
        axis: the axis over which to demodulate
        fd: the demodulation frequency [Hz] (float or array: len(array) = data.shape[axis])
        fs: sampling frequency along 'axis'
        mode: 'numpy' or 'xarray'
        filter: the type of low pass filter used
    Outputs:
        rf: real component of rf signal
    """

    if mode == 'numpy':
        try:
            # upsample data before remodulation
            from pyus.improc.sampling import upsample
            usfs = np.ones(shape=len(I.shape), dtype=int)
            usfs[axis] = usf
            I = upsample(I, usfs=usfs, window=window, mode='numpy')
            Q = upsample(Q, usfs=usfs, window=window, mode='numpy')

            # remodulate the data
            from . _bmfrm_helpers import _remodulate_numpy
            return _remodulate_numpy(I, Q, fd, fs*usf, fc, tstart, axis, filter=filter)
        except Exception as e:
            logger.warn("Error occured when demodulating in numpy mode")
            print(e)
            raise e
    elif mode == 'xarray':
        try:
            from . _bmfrm_helpers import _remodulate_xarray
            return _remodulate_xarray(I, Q, fd, fs, tstart, axis, filter=filter)
        except Exception as e:
            logger.warn("Error occured when demodulating in xarray mode")
            raise e
    else:
        raise Exception(f"{mode} is not a valid demodulation call. Must be 'numpy' or 'xarray'")



