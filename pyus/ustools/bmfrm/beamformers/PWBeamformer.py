import logging
import numpy as np
from pyus.ustools.bmfrm.beamformers.Beamformer import Beamformer
logger = logging.getLogger(__name__)

class PWBeamformer(Beamformer):
    """Plane wave beamformer
    """
    def __init__(self, axial_in, lateral_in, axial_out, lateral_out, alpha, c0=1540, fnum=None, dt_peak=0.0, axial_mode='spacespace'):
        super().__init__()
        self.axial_in = axial_in
        self.N_ax_in = len(axial_in)
        self.lateral_in = lateral_in
        self.N_lat_in = len(lateral_in)

        # Transmit parameters
        self.alpha = alpha
        self.c0 = c0
        self.fnum = fnum
        self.dt_peak = dt_peak
        self.axial_mode = axial_mode

        # output matrix 
        self.axial_out = axial_out
        self.N_ax_out = len(axial_out)
        self.lateral_out = lateral_out
        self.N_lat_out = len(lateral_out)
        self.generate()

    def generate(self):
        """Calculate the timepoints to include for the given parameters"""
        # Make a matrix of delay tabs indexed by output lateral and axial indices
        tau_r = np.full(shape=(self.N_ax_out, self.N_lat_out), fill_value=None)
        
        # generate conversion factors to put all cordinates into space
        lat_in = self.lateral_in        # lateral output range
        lat_out = self.lateral_out      # lateral output range
        ax_out = self.axial_out         # axial output range
        ax_min = np.min(self.axial_in)  # min tau of input data [s]
        ax_max = np.max(self.axial_in)  # max tau of input data [s]

        # Convert coordinates to space and boundaries to time
        if self.axial_mode == 'spacetime':
            ax_out = ax_out*self.c0/2
            ax_min = 2*ax_min/self.c0
            ax_max = 2*ax_max/self.c0
        elif self.axial_mode == 'spacespace':
            ax_min = 2*ax_min/self.c0
            ax_max = 2*ax_max/self.c0
        elif self.axial_mode == 'timetime':
            ax_out = ax_out*self.c0/2
            

        # If fnum=None, maximal focussing. Else, use up to as many elements as the f-number allows
        for col, lat_val in enumerate(lat_out):
            for row, ax_val in enumerate(ax_out):
                # find range of lateral input positions to include
                if self.fnum is not None:
                    if ax_val == 0:
                        lat_inds = np.argsort(np.abs(lat_in-lat_val))[0]
                        lat = np.array([lat_in[lat_inds]])
                    else:
                        # find the lateral input locations within the apperature
                        lat_inds = np.where(np.abs((lat_in-lat_val)/ax_val) <= self.fnum/2)
                        lat = np.array([lat_in[lat_inds]])
                else:
                    lat = np.array(lat_in)

                # Only include time delays within boundaries of input array
                if len(lat) == 0:
                    tau_r[row, col] = None

                else:
                    # Time delay from emmission to coincidence with the given point
                    tau_ec = (ax_val*np.cos(self.alpha) + lat_val*np.sin(self.alpha))/self.c0

                    # Time delay from the coincidence to the recieve element
                    tau_cr = np.sqrt(ax_val**2 + (lat-lat_val)**2)/self.c0

                    # Sum the distances from emmission to coincidence to recieve
                    tau = tau_ec + tau_cr + 2*self.dt_peak # 2*dt_peak is to include a time delay for the distance to the pulse

                    # Only include times corresponding to the range of the input data
                    inc = (tau >= ax_min) | (tau <= ax_max)
                    if len(inc) == 0:
                        tau_r[row, col] = None
                    else:
                        tau_r[row, col] = {'tau':tau[inc], 'lat':lat[inc]}
    
        self.tau_r = tau_r

    def __call__(self, data, fill_val=np.nan, k=int(3)):
        """beamforms data following the the parameters defined on object generation
        
        Equivalent to calling BF.form(*)

        Parameters:
            data: an N_ax_in by N_lat_in array. Must match size of input parameters

        Returns:
            formed: beamformed data

        Raises:
            ValueError: if data is not 2D
            ValueError: if data is not of the appropriate input dimensions
        """
        return self.form(data, fill_val=fill_val, k=k)

    def form(self, data, fill_val=np.nan, k=int(3)):
        """beamforms data following the the parameters defined on object generation
        
        Parameters:
            data: an N_ax_in by N_lat_in array. Must match size of input parameters
            fill_val: fill value for empty slots in data,
            k: order of polynomial interpolation used in beamforming. Default is bicubic (3)

        Returns:
            formed: beamformed data with 

        Raises:
            ValueError: if data is not 2D
            ValueError: if data is not of the appropriate input dimensions
        """
        if np.ndim(data) != 2:
            raise ValueError(f"data must be 2D but is {np.ndim(data)}D.")
        if (data.shape[0] != self.N_ax_in) or (data.shape[1] != self.N_lat_in):
            raise ValueError(f"data input for this beamformer must be {self.N_ax_in} by {self.N_ax_in} but was {data.shape[0]} by {data.shape[1]}")
        
        # Expected runtime warnings when all input values are NAN
        # This will occur when all requested beamformed data is outside of range of input data
        formed = np.zeros((self.N_ax_out, self.N_lat_out))

        # Convert axial coordinates to time
        ax_in = self.axial_in
        if self.axial_mode == 'spacespace':
            ax_in = 2*ax_in/self.c0
        if self.axial_mode == 'spacetime':
            ax_in = 2*ax_in/self.c0

        # generate interpolation object
        from scipy.interpolate import RectBivariateSpline as RBS
        f = RBS(ax_in, self.lateral_in, data, kx=k, ky=k)

        for row in range(self.N_ax_out):
            for col in range(self.N_lat_out):
                if self.tau_r[row,col] is None:
                    formed[row,col] = fill_val 
                # Load the time delays and lateral poitions
                tau = self.tau_r[row,col]['tau']
                lat = self.tau_r[row,col]['lat']

                formed[row, col] = np.mean(f(tau, lat, grid=False))
        return formed